﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Timer
{
    private float expirationTime = -1;

    public bool isOver(float delay, float deltaTime)
    {
        if (expirationTime > 0)
        {
            expirationTime -= deltaTime;
            return false;
        } else
        {
            expirationTime = delay;
            return true;
        }
    }
}

