﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class SFXSingleton : Singleton<SFXSingleton>
{
  public AudioClip eatClip;
  public AudioClip loseClip;
  public AudioClip buttonClip;
  public AudioClip winClip;

  void Awake()
  {
    GetComponent<AudioSource>().playOnAwake = false;
    GetComponent<AudioSource>().loop = false;
    GetComponent<AudioSource>().Stop();
  }

  void Start() {
		GetComponent<AudioSource>().enabled = PlayerPrefs.GetInt(Strings.Options.Sound.SFX) != 0;
  }

  void Play(AudioClip clip)
  {
		if (GetComponent<AudioSource>().enabled) {
			GetComponent<AudioSource>().Stop ();
			GetComponent<AudioSource>().clip = clip;
			GetComponent<AudioSource>().Play ();
		}
  }

  public void PlayEat()
  {
    Play(eatClip);
  }

  public void PlayLose()
  {
    Play(loseClip);
  }

  public void PlayButton()
  {
    Play(buttonClip);
  }

  public void PlayWin()
  {
    Play(winClip);
  }
}
