﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class MusicSingleton : Singleton<MusicSingleton>
{
	public AudioClip[] Music;
	int currentClipIndex = -1;

	void Awake ()
	{
		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().loop = false;
		GetComponent<AudioSource> ().Stop ();
	}

	void Start ()
	{
		GetComponent<AudioSource> ().enabled = PlayerPrefs.GetInt (Strings.Options.Sound.MUSIC) != 0;

		// Improve random generator
		Random.seed = (int)System.DateTime.Now.Ticks;

		if (Music.Length > 0) {
			currentClipIndex = Random.Range (0, Music.Length);

			if (GetComponent<AudioSource> ().enabled && !GetComponent<AudioSource> ().isPlaying) {
				PlayNextClip ();
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GetComponent<AudioSource> ().enabled == true && Music.Length > 0 && !GetComponent<AudioSource> ().isPlaying) {
			PlayNextClip ();
		}
	}

	void PlayNextClip ()
	{
		++currentClipIndex;
		if (currentClipIndex < 0 ||
			currentClipIndex >= Music.Length) {
			currentClipIndex = 0;
		}

		AudioClip currentClip = Music [currentClipIndex];
		GetComponent<AudioSource> ().clip = currentClip;
		GetComponent<AudioSource> ().Play ();
	}
}
