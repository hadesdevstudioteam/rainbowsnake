﻿using UnityEngine;
using System.Collections;

abstract public class Strings
{

    abstract public class Options
    {
        public static string CONTROL = "OptionControl";
        abstract public class Control
        {
            public enum TYPE
            {
                KEYBOARD = 0,
                MOUSE = 1,
                TOUCH_SWIPE = 2,
                TOUCH_MOVE = 3,
                JOYSTICK = 4
            }
            public static TYPE DEF_VALUE = TYPE.TOUCH_SWIPE;
        }

        // class for option snake
        abstract public class Snake
        {
            // class for option snake color reverse
            public static string COLOR_REVERSE = "OptionSnakeColorReverse";
            abstract public class ColorReverse
            {
                public static bool DEF_VALUE = false;
            }

            // class for option snake speed
            public static string SPEED = "OptionSnakeSpeed";
            abstract public class Speed
            {
                public static float MIN_VALUE = 1.0f;
                public static float MAX_VALUE = 5.0f;
                public static float DEF_VALUE = MIN_VALUE;
            }
        }

        abstract public class Sound
        {
            public static string MUSIC = "OptionSoundMusic";
            abstract public class Music
            {
                public static int DEF_VALUE = 1;
            }

            public static string SFX = "OptionSoundSfx";
            abstract public class Sfx
            {
                public static int DEF_VALUE = 1;
            }
        }
    }

    abstract public class Prefs
    {
        // class for prop level
        public static string LEVEL = "Level";
        abstract public class Level
        {
            public static int MIN_VALUE = 1;
            public static int MAX_VALUE = 100;
            public static int DEF_VALUE = MIN_VALUE;
        }

        // class for prop life
        public static string LIFE = "Life";
        abstract public class Life
        {
            public static int MIN_VALUE = 0;
            public static int MAX_VALUE = 10;
            public static int DEF_VALUE = 5;
        }

        // class for prop temp score
        public static string TEMP_SCORE = "TempScore";
        abstract public class TempScore
        {
            public static int DEF_VALUE = 0;
        }
        
        // class for prop final score
        public static string FINAL_SCORE = "FinalScore";
        abstract public class FinalScore
        {
            public static int DEF_VALUE = 0;
        }
    }

    abstract public class Scenes
    {
        public static string LOBBY = "LobbyScene";
        public static string GAME = "GameScene";
    }

    abstract public class GameObjects
    {
        public static string EVENT_MOCK_OBJECT = "event_mock_object";
        public static string SINGLETON_CONTAINER = "SingletonContainer";
        public static string IMAGE_GRADIENT = "ImageGradient";
        public static string SLIDER_FOOD_LEFT = "FoodLeftSlider";

        abstract public class GameMenu
        {
            public static string CANVAS_MENU = "Canvas-Menu";
            public static string BUTTON_RESUME = "BtnResume";
            public static string BUTTON_RESTART = "BtnRestart";
            public static string BUTTON_NEXTLEVEL = "BtnNextLevel";
        }
    }
}
