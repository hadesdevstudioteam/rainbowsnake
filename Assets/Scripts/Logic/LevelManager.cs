﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{

    private static LevelManager _instance;
    public static LevelManager I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType<LevelManager>();
                if (!_instance)
                {
                    GameObject container = GameObject.Find(Strings.GameObjects.SINGLETON_CONTAINER);
                    if (container == null)
                    {
                        container = new GameObject();
                        container.name = Strings.GameObjects.SINGLETON_CONTAINER;
                        DontDestroyOnLoad(container);
                    }
                    
                    _instance = container.AddComponent<LevelManager>();
                }
            }

            return _instance;
        }
    }

    private static Color[] palette = 
		{
			new Color(0.0f, 0.0f, 1.0f), // blue
			new Color(0.0f, 0.5f, 1.0f), // aqua
			new Color(0.0f, 1.0f, 1.0f), // cyan

			new Color(0.0f, 1.0f, 0.5f), // azure
			new Color(0.0f, 1.0f, 0.0f), // green
			new Color(0.5f, 1.0f, 0.0f), // green yellow

			new Color(1.0f, 1.0f, 0.0f), // yellow
			new Color(1.0f, 0.5f, 0.0f), // orange
			new Color(1.0f, 0.0f, 0.0f), // red
			
			new Color(1.0f, 0.0f, 0.5f), // pink
			new Color(1.0f, 0.0f, 1.0f), // magenta
			new Color(0.5f, 0.0f, 1.0f), // purple
		};

    private int level = Strings.Prefs.Level.DEF_VALUE;
    private int life = Strings.Prefs.Life.DEF_VALUE;

    private float maxOutRange;
    private float maxInnerRange;

    private int maxSnakeLength;
    private int maxFoodCount;
    private int maxPaletteColor;

    private int currentSnakeLength = 0;
    private float currentEvaluate = 0.0f;

    private Gradient gradient;

    GameObject imageGradient;
    GameObject menuCanvas;
    GameObject resumeButton;
    GameObject restartButton;
    GameObject nextLevelButton;
    GameObject foodLeftValueSlider;


    public void GameOver()
    {
        level = Strings.Prefs.Level.DEF_VALUE;
        PlayerPrefs.SetInt(Strings.Prefs.LEVEL, level);
        life = Strings.Prefs.Life.DEF_VALUE;
        PlayerPrefs.SetInt(Strings.Prefs.LIFE, life);
        ShowMenuExitToMainMenu();
    }

    public void FailLevel()
    {
        --life;
        if (life < Strings.Prefs.Life.MIN_VALUE)
        {
            ScoreManager.I.UpdateFinalScore();
            GameOver();
        } else
        {
            PlayerPrefs.SetInt(Strings.Prefs.LIFE, life);
            ShowMenuRestartLevel();
        }
    }

    public void NextLevel()
    {
        ScoreManager.I.UpdateFinalScore();

        if (life < Strings.Prefs.Life.MAX_VALUE)
        {
            ++life;
            PlayerPrefs.SetInt(Strings.Prefs.LIFE, life);
        }

        if (level < Strings.Prefs.Level.MAX_VALUE)
        {
            ++level;
            PlayerPrefs.SetInt(Strings.Prefs.LEVEL, level);
            ShowMenuNextLevel();
        } else
        {
            // complete game
            GameOver();
        }
    }

    void Start()
    {
        imageGradient = GameObject.Find(Strings.GameObjects.IMAGE_GRADIENT);
        menuCanvas = GameObject.Find(Strings.GameObjects.GameMenu.CANVAS_MENU);
        resumeButton = GameObject.Find(Strings.GameObjects.GameMenu.BUTTON_RESUME);
        restartButton = GameObject.Find(Strings.GameObjects.GameMenu.BUTTON_RESTART);
        nextLevelButton = GameObject.Find(Strings.GameObjects.GameMenu.BUTTON_NEXTLEVEL);
        foodLeftValueSlider = GameObject.Find(Strings.GameObjects.SLIDER_FOOD_LEFT);
        StartLevel();
    }

    private void StartLevel()
    {
        ScoreManager.I.CleanTempScore();

        HideMenu();

        InitValues();
        
        gradient = CreateGradient();
        
        WorldManager.I.Clear();
        
        SetCurrentSnakeLength(0);
        
        imageGradient.GetComponent<GradientBarScript>().OnLevelChanged(gradient);
        
        PlayerManager.I.OnLevelChanged();
        SetCurrentSnakeLength(1);
        
        // always clean food
        FoodManager.I.ClearFood();
        FoodManager.I.GenerateColors(GetMaxSnakeLength(), GetMaxFoodCount());
        FoodManager.I.GenerateFood(GetMaxFoodCount());
    }

    private void InitValues()
    {
        level = PlayerPrefs.GetInt(Strings.Prefs.LEVEL);
        life = PlayerPrefs.GetInt(Strings.Prefs.LIFE);
        maxSnakeLength = (int)(9.59184f + 0.408163f * level);
        maxFoodCount = (int)(1.91837f + 0.0816327f * level);
        maxInnerRange = 0.252424f - 0.00242424f * level;
        maxOutRange = 0.45404f - 0.0040404f * level;
        maxPaletteColor = (int)(1.93878f + 0.0612245f * level);
        foodLeftValueSlider.GetComponent<FoodLeftValue>().Init(maxSnakeLength);
    }

    private Gradient CreateGradient()
    {		
        
        // Improve random generator
        Random.seed = (int)System.DateTime.Now.Ticks;

        List<Color> listPalette = new List<Color>(palette);
        int index = Random.Range(0, listPalette.Count);
        int index_next = listPalette.Count / maxPaletteColor;

        // reverse?
        if (Random.value > 0.5f)
            listPalette.Reverse();

        Gradient g = new Gradient();
        GradientColorKey[] gck = new GradientColorKey[maxPaletteColor];
        GradientAlphaKey[] gak = new GradientAlphaKey[maxPaletteColor];
        float t = 0.0f;

        for (int i = 0; i < maxPaletteColor; ++i)
        {
            t = (float)i / (float)(maxPaletteColor - 1);

            gck [i].color = listPalette [index];
            gck [i].time = t;
            gak [i].alpha = 1.0f;
            gak [i].time = t;

            // recycle palette
            index += index_next;
            if (index >= listPalette.Count)
                index -= listPalette.Count;
        }
        g.SetKeys(gck, gak);

        return g;
    }

    public int GetMaxSnakeLength()
    {
        return maxSnakeLength;
    }

    public int GetCurrentSnakeLength()
    {
        return currentSnakeLength;
    }

    public void IncreaseSnakeLength()
    {
        SetCurrentSnakeLength(GetCurrentSnakeLength() + 1);
    }

    public int GetMaxFoodCount()
    {
        return maxFoodCount;
    }

    public void SetCurrentSnakeLength(int length)
    {
        currentSnakeLength = length;
        currentEvaluate = (float)length / (float)(maxSnakeLength - 1);
        imageGradient.GetComponent<GradientBarScript>().OnSnakeLengthChanged(currentEvaluate);
        foodLeftValueSlider.GetComponent<FoodLeftValue>().UpdateFood(currentSnakeLength);
    }

    public bool IsLevelWon()
    {
        return (GetCurrentSnakeLength() >= GetMaxSnakeLength());
    }

    public Color GetCurrentColor()
    {
        return gradient.Evaluate(currentEvaluate);
    }

    public Color GetRandomColor(float evaluate)
    {
        float randColor = Random.Range(maxInnerRange, maxOutRange);

        bool leftOutPalette = (evaluate - randColor) <= 0.0f;
        bool rightOutPalette = (evaluate + randColor) >= 1.0f;

        // if last color in palette, then always use backward colors
        int coeff = (rightOutPalette)
			? -1
        // generate backward color if possible
			: (leftOutPalette)
				? 1
        // range fit, choose random direction
				: (Random.Range(0, 2) == 0)
					? -1
					: 1;

        randColor *= (float)coeff;
        randColor += evaluate;

        return gradient.Evaluate(randColor);
    }

    public int GetCurrentLevel()
    {
        return level;
    }

    private void ShowMenu(bool isVisible, bool resume, bool restart, bool nextLevel)
    {
        if (isVisible == menuCanvas.activeSelf)
            return;
        resumeButton.SetActive(resume);
        restartButton.SetActive(restart);
        nextLevelButton.SetActive(nextLevel);
        menuCanvas.SetActive(isVisible);
    }

    public void ShowMenuNextLevel()
    {
        ShowMenu(true, false, false, true);
    }

    public void ShowMenuResumeLevel()
    {
        ShowMenu(true, true, false, false);
    }

    public void ShowMenuRestartLevel()
    {
        ShowMenu(true, false, true, false);
    }

    public void ShowMenuExitToMainMenu()
    {
        ShowMenu(true, false, false, false);
    }

    public void HideMenu()
    {
        ShowMenu(false, false, false, false);
    }

    void OnApplicationFocus(bool focusStatus)
    {
        if (!focusStatus)
        {
            ShowMenuResumeLevel();
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            ShowMenuResumeLevel();
        }
    }

    public bool IsPaused()
    {
        return menuCanvas.activeSelf;
    }
}
