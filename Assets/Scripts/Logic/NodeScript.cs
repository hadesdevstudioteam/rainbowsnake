﻿using UnityEngine;
using System.Collections;


public class NodeScript : MonoBehaviour
{

  private GameObject child;

  public void DestroyNode()
  {
    if (child != null)
      child.GetComponent<NodeScript>().DestroyNode();
    Destroy(gameObject);
  }

  public void Move(Vector3 newPosition)
  {
    Vector3 oldPosition = transform.position;
    Vector3 direction = newPosition - oldPosition;

    WorldManager.I.OnMoveObjectFrom(oldPosition);
    WorldManager.I.OnMoveObjectTo(newPosition, NodeType.NOT_EAT | NodeType.SNAKE);
    transform.Translate(direction);

    if (child)
      child.GetComponent<NodeScript>().Move(oldPosition);
  }

  public void SetChild(GameObject child)
  {
    this.child = child;
  }

  public void UpdateColor(Color newColor)
  {
    if (PlayerPrefs.GetInt(Strings.Options.Snake.COLOR_REVERSE) == 1)
    {
      gameObject.GetComponent<Renderer>().material.color = newColor;
    } else
    {
      if (child)
      {
        gameObject.GetComponent<Renderer>().material.color = child.gameObject.GetComponent<Renderer>().material.color;
        child.GetComponent<NodeScript>().UpdateColor(newColor);
      } else
      {
        gameObject.GetComponent<Renderer>().material.color = newColor;
      }
    }
  }

}
