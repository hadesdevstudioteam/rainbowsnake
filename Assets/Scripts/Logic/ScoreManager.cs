﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{

    private static ScoreManager _instance;
    public static ScoreManager I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType<ScoreManager>();
                if (!_instance)
                {
                    GameObject container = GameObject.Find(Strings.GameObjects.SINGLETON_CONTAINER);
                    if (container == null)
                    {
                        container = new GameObject();
                        container.name = Strings.GameObjects.SINGLETON_CONTAINER;
                        DontDestroyOnLoad(container);
                    }

                    _instance = container.AddComponent<ScoreManager>();
                }
            }

            return _instance;
        }
    }

    private int scoreMultiplySeconds = 3;
    private int score = 0;
    private float expirationTime;

    void Update()
    {
        expirationTime -= Time.deltaTime;
    }

    private void SetTempScore(int value)
    {
        score = value;
        PlayerPrefs.SetInt(Strings.Prefs.TEMP_SCORE, score);
    }

    public void IncScore()
    {
        float reduce = (PlayerPrefs.GetInt(Strings.Options.Snake.COLOR_REVERSE) == 0) ? 0.75f : 1.0f;
        int score = PlayerPrefs.GetInt(Strings.Prefs.TEMP_SCORE);
        score += (int)(reduce * 100.0f * (1.0f + Mathf.Max(0, expirationTime)));
        SetTempScore(score);
        expirationTime = scoreMultiplySeconds;
    }

    public void CleanTempScore()
    {
        PlayerPrefs.SetInt(Strings.Prefs.TEMP_SCORE, Strings.Prefs.TempScore.DEF_VALUE);
    }

    public void UpdateFinalScore()
    {
        int total_score = PlayerPrefs.GetInt(Strings.Prefs.TEMP_SCORE);
        total_score += PlayerPrefs.GetInt(Strings.Prefs.FINAL_SCORE);
        PlayerPrefs.SetInt(Strings.Prefs.FINAL_SCORE, total_score);
    }
}
