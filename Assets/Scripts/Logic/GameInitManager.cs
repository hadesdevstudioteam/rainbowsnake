﻿using UnityEngine;
using System.Collections;

public class GameInitManager : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        // check preference and set default values
        if (!PlayerPrefs.HasKey(Strings.Prefs.LEVEL))
            PlayerPrefs.SetInt(Strings.Prefs.LEVEL, Strings.Prefs.Level.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Prefs.TEMP_SCORE))
            PlayerPrefs.SetInt(Strings.Prefs.TEMP_SCORE, Strings.Prefs.TempScore.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Prefs.FINAL_SCORE))
            PlayerPrefs.SetInt(Strings.Prefs.FINAL_SCORE, Strings.Prefs.FinalScore.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Prefs.LIFE))
            PlayerPrefs.SetInt(Strings.Prefs.LIFE, Strings.Prefs.Life.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Options.Snake.SPEED))
            PlayerPrefs.SetFloat(Strings.Options.Snake.SPEED, Strings.Options.Snake.Speed.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Options.Snake.COLOR_REVERSE))
            PlayerPrefs.SetInt(Strings.Options.Snake.COLOR_REVERSE, Strings.Options.Snake.ColorReverse.DEF_VALUE ? 1 : 0);
        if (!PlayerPrefs.HasKey(Strings.Options.CONTROL))
            PlayerPrefs.SetInt(Strings.Options.CONTROL, (int)Strings.Options.Control.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Options.Sound.MUSIC))
            PlayerPrefs.SetInt(Strings.Options.Sound.MUSIC, Strings.Options.Sound.Music.DEF_VALUE);
        if (!PlayerPrefs.HasKey(Strings.Options.Sound.SFX))
            PlayerPrefs.SetInt(Strings.Options.Sound.SFX, Strings.Options.Sound.Sfx.DEF_VALUE);
    }

}
