﻿using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    // set snake speed
    // 5 - 0.1
    // 4 - 0.2
    // 3 - 0.3
    // 2 - 0.4
    // 1 - 0.5
    float stepDelay;

    public GameObject playerPrefab;
    private GameObject snake;
    private NodeScript snakeScript;

    private Control.Event controlEvent;

    private static IntVector2 controlDirection;
    
    private static IntVector2 headDirection;

    private static GameObject head;

    Timer timerForMove = new Timer();

    public static PlayerManager I { get; private set; }
    void Awake()
    {
        if (I == null)
            I = this;
    }

    public void OnLevelChanged()
    {
        //build snake
        Vector2 newPos = WorldManager.I.GetRandomFreeNode();
        WorldManager.I.OnMoveObjectTo(newPos, NodeType.NOT_EAT | NodeType.SNAKE);
        snake = Instantiate(playerPrefab, newPos, Quaternion.identity) as GameObject;
        snake.GetComponent<Renderer>().material.color = LevelManager.I.GetCurrentColor();
        snakeScript = snake.GetComponent<NodeScript>();
        head = snake;

        // reset control
        headDirection = IntVector2.zero;
        controlDirection = IntVector2.zero;
    }

    private void SetDirection()
    {
        if (!LevelManager.I.IsPaused() && timerForMove.isOver(stepDelay, Time.deltaTime))
        {
            SetHeadDirection();

            if (headDirection != IntVector2.zero)
                DoActionByDirection(snake.transform.position + headDirection);
        }
    }

    private void SetHeadDirection()
    {
        // waste CPU on start game!
        if (controlDirection == IntVector2.zero)
            return;

        // set head direction
        headDirection = controlDirection;
    }

    protected void DoActionByDirection(Vector3 direction)
    {
        NodeType frontNode = WorldManager.I.GetNodeType(direction);

        if (NodeType.EMPTY != frontNode)
        {
            // always clean food
            FoodManager.I.ClearFood();

            if (NodeType.EAT == frontNode)
            {
                SFXSingleton.Get().PlayEat();
                GameObject child = snake;

                ScoreManager.I.IncScore();
                snake = Instantiate(playerPrefab, direction, Quaternion.identity) as GameObject;
                snakeScript = snake.GetComponent<NodeScript>();

                snakeScript.SetChild(child);

                snakeScript.UpdateColor(LevelManager.I.GetCurrentColor());

                LevelManager.I.IncreaseSnakeLength();
                if (LevelManager.I.IsLevelWon())
                {
                    SFXSingleton.Get().PlayWin();
                    LevelManager.I.NextLevel();
                } else
                {
                    WorldManager.I.OnMoveObjectTo(direction, NodeType.NOT_EAT | NodeType.SNAKE);
                    FoodManager.I.GenerateFood(LevelManager.I.GetMaxFoodCount());
                }

                return;
            } else
            {
                SFXSingleton.Get().PlayLose();
                snakeScript.DestroyNode();
                LevelManager.I.FailLevel();
                return;
            }
        }

        snakeScript.Move(direction);
    }

    public GameObject getHead()
    {
        return head;
    }

    void Start()
    {
        stepDelay = 0.5f - (PlayerPrefs.GetFloat(Strings.Options.Snake.SPEED) - 1.0f) * 0.1f;

        controlEvent = Control.Event.CreateEvent((Strings.Options.Control.TYPE)PlayerPrefs.GetInt(Strings.Options.CONTROL));
        headDirection = IntVector2.zero;
        controlDirection = IntVector2.zero;
    }

    void Update()
    {
        if (LevelManager.I.IsLevelWon())
        {
            snakeScript.DestroyNode();
            gameObject.SetActive(false);
            return;
        }

        // don't eat this SHIT!
        if (LevelManager.I.IsPaused() || snake == null)
            return;

        controlDirection = controlEvent.GetValidDirection(headDirection);

        SetDirection();
    }
}
