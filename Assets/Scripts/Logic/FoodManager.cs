﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FoodManager : MonoBehaviour
{
    public GameObject foodPrefab;
    static Dictionary<Vector2, GameObject> foods = new Dictionary<Vector2, GameObject>();
    static List<List<Color>> colors = new List<List<Color>>();

    private FoodManager()
    {
    }

    public static FoodManager I { get; private set; }

    void Awake()
    {
        if (I == null)
            I = this;
    }

    public void ClearFood()
    {
        // clean old foods
        foreach (var p in foods)
        {
            WorldManager.I.OnMoveObjectFrom(p.Key);
            Destroy(p.Value);
        }
        foods.Clear();
    }

    public void GenerateColors(int maxSnakeLength, int maxFoodCount)
    {
        float evaluate;
        int index;

        // Improve random generator
        Random.seed = (int)System.DateTime.Now.Ticks;

        colors.Clear();
        for (int i = 0; i < maxSnakeLength; ++i)
        {
            colors.Add(new List<Color>(maxFoodCount));
            index = colors.Count - 1;
            for (int j = 0; j < maxFoodCount; ++j)
            {
                evaluate = (float)(i + 1) / (float)(maxSnakeLength - 1);
                colors[index].Add( LevelManager.I.GetRandomColor( evaluate ) );
            }
        }
    }

    public void GenerateFood(int maxFoodCount)
    {
        List<Vector2> listFree = WorldManager.I.GetFreeNodes();

        Vector2 newPos;
        GameObject food;
        int index;

        // Improve random generator
        Random.seed = (int)System.DateTime.Now.Ticks;

        /* generate all max wrong foods */
        for (int i = 0; i < maxFoodCount; ++i)
        {
            index = Random.Range(0, listFree.Count);

            newPos = listFree [index];
            food = Instantiate(foodPrefab, newPos, Quaternion.identity) as GameObject;
            food.GetComponent<Renderer>().material.color = GetColor(i);
            foods.Add(newPos, food);

            WorldManager.I.OnMoveObjectTo(newPos, NodeType.NOT_EAT);

            listFree.RemoveAt(index);
        }

        /* force set first food as right, if Unity stuck */
        var tmpFood = new KeyValuePair<Vector2, GameObject>( foods.Keys.First(), foods.Values.First() );

        var tmpPos   = tmpFood.Key;
        var tmpGO    = tmpFood.Value;
        var tmpColor = tmpGO.GetComponent<Renderer>().sharedMaterial.color;

        WorldManager.I.OnMoveObjectTo(tmpPos, NodeType.EAT);
        tmpGO.GetComponent<Renderer>().sharedMaterial.color = LevelManager.I.GetCurrentColor();

        /* set right food */
        foreach (var p in foods)
        {
            if (WorldManager.I.IsSnakeCanReachNode(p.Key) && !tmpPos.Equals(p.Key) )
            {
                WorldManager.I.OnMoveObjectTo(tmpPos, NodeType.NOT_EAT);
                tmpGO.GetComponent<Renderer>().sharedMaterial.color = tmpColor;
                WorldManager.I.OnMoveObjectTo(p.Key, NodeType.EAT);
                p.Value.GetComponent<Renderer>().sharedMaterial.color = LevelManager.I.GetCurrentColor();
                return;
            }
        }
    }

    private Color GetColor(int index)
    {
        return colors[ LevelManager.I.GetCurrentSnakeLength()-1 ][index];
    }
}
