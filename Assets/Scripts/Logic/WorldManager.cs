﻿using UnityEngine;
using System.Collections.Generic;

[System.Flags]
public enum NodeType
{
    EMPTY 	= 0,
    EAT		= 1 << 0,
    NOT_EAT = 1 << 1,
    SNAKE   = 1 << 2,
    OUT     = 1 << 3
}

//singleton with force instantiation
public class WorldManager : MonoBehaviour
{
    public bool debug = false;
    static int worldSize = 11;

    private NodeType[,] matrix;

    private int worldLengthX = 0;
    private int worldLengthY = 0;

    private int freeNodeLeft = 0;

    private LeeAlgorithm leeAlgo;

    public static WorldManager I { get; private set; }

    void Awake()
    {
        if (I == null)
            I = this;
        worldLengthX = worldSize;
        worldLengthY = worldSize;
        matrix = new NodeType[worldLengthX, worldLengthY];
        
        leeAlgo = new LeeAlgorithm(worldLengthX, worldLengthY);

        Clear();
    }

    public void Clear()
    {
        freeNodeLeft = worldLengthX * worldLengthY;
        for (int x = worldLengthX - 1; x >= 0; --x)
            for (int y = worldLengthY - 1; y >= 0; --y)
            {
                matrix [x, y] = NodeType.EMPTY;
                leeAlgo.SetCellAsFree(x, y);
            }
    }

    private bool isInWorld(IntVector2 point)
    {
        return (0 <= point.x && point.x < worldLengthX) && (0 <= point.y && point.y < worldLengthY);
    }

    public NodeType GetNodeType(IntVector2 point)
    {
        return isInWorld(point) 
            ? matrix [point.x, point.y] 
            : (NodeType.NOT_EAT | NodeType.OUT);
    }

    private bool CheckNearSnakeNode(IntVector2 point)
    {

        int center_x = point.x;
        int center_y = point.y;

        // check in x-coordinate
        int new_center_x;
        for (int x = -2; x < 3; ++x)
        {
            // skip self
            if (x == 0) 
                continue;
            // set new cell x-coordinate
            new_center_x = center_x + x;
            // skip out of matrix x-coordinate;
            if ((new_center_x < 0) || (new_center_x >= worldLengthX)) 
                continue;
            if ((matrix [new_center_x, center_y] & NodeType.SNAKE) == NodeType.SNAKE)
                return false;
        }

        // check in y-coordinate
        int new_center_y;
        for (int y = -2; y < 3; ++y)
        {
            // skip self
            if (y == 0)
                continue;
            // set new cell y-coordinate
            new_center_y = center_y + y;
            // skip out of matrix y-coordinate;
            if ((new_center_y < 0) || (new_center_y >= worldLengthY))
                continue;
            if ((matrix [center_x, new_center_y] & NodeType.SNAKE) == NodeType.SNAKE)
                return false;
        }

        return true;
    }

    public List<Vector2> GetFreeNodes()
    {
        List<Vector2> listFree = new List<Vector2>(worldLengthX * worldLengthY);

        // if free node exists
        if (isFreeNodeLeft())
        {
            IntVector2 tmpVec = new IntVector2();

            for (int x = worldLengthX - 1; x >= 0; --x)
                for (int y = worldLengthY - 1; y >= 0; --y)
                    if (NodeType.EMPTY == matrix [x, y])
                    {
                        tmpVec.x = x;
                        tmpVec.y = y;
                        if (CheckNearSnakeNode(tmpVec))
                        {
                            listFree.Add(tmpVec.MoveTo(0.5f, 0.5f));
                        }
                    }
        }
        // save little memory
        listFree.TrimExcess();

        return listFree;
    }

    public bool isFreeNodeLeft()
    {
        return (freeNodeLeft > 0);
    }

    public void OnMoveObjectTo(IntVector2 toPosition, NodeType nodeType)
    {
        if (isInWorld(toPosition))
        {
            matrix [toPosition.x, toPosition.y] = nodeType;
            leeAlgo.SetCellAsBusy(toPosition.x, toPosition.y);
            --freeNodeLeft;
        }
    }

    public void OnMoveObjectFrom(IntVector2 fromPosition)
    {
        if (isInWorld(fromPosition))
        {
            matrix [fromPosition.x, fromPosition.y] = NodeType.EMPTY;
            leeAlgo.SetCellAsFree(fromPosition.x, fromPosition.y);
            ++freeNodeLeft;
        }
    }
    
    public Vector2 GetRandomFreeNode()
    {
        // not chance for generate random vector
        if (!isFreeNodeLeft())
            return IntVector2.zero;

        IntVector2 rand = new IntVector2();

        // Improve random generator
        Random.seed = (int)System.DateTime.Now.Ticks;

        // TODO: it can take long long time...
        while (true)
        {

            rand.x = Random.Range(0, worldLengthX - 1);
            rand.y = Random.Range(0, worldLengthY - 1);

            if (NodeType.EMPTY == GetNodeType(rand))
                return rand.MoveTo(0.5f, 0.5f);
        }

    }

    // based on Lee Algorithm
    public bool IsSnakeCanReachNode(IntVector2 targetPos)
    {
        IntVector2 headPos = PlayerManager.I.getHead().transform.position;
        return leeAlgo.find(headPos.x, headPos.y, targetPos.x, targetPos.y);
    }

    void OnGUI()
    {
        if (!debug)
            return;

        for (int x = 0; x < worldSize; ++x)
        {
            for (int y = 0; y < worldSize; ++y)
            {

                if (NodeType.EMPTY == matrix [x, y])
                {
                    Camera cam = GameObject.FindObjectOfType<Camera>();
                    Vector3 screenPos = cam.WorldToScreenPoint(new Vector3(x, y, -1));
                    Rect rect = new Rect(screenPos.x, Screen.height - screenPos.y - 20, screenPos.x, Screen.height - screenPos.y);
                    GUI.Label(rect, matrix [x, y].ToString());
                }
            }
        }
    }
}