﻿using UnityEngine;
using System.Collections;

namespace Control
{
	public class JoystickEvent : Event {

		private float delta_x;
		private float delta_y;
		private bool delta_x_sign;
		private bool delta_y_sign;

		protected override void GetEvent()
		{
			delta_x = Input.GetAxis ("Horizontal");
			delta_y = Input.GetAxis ("Vertical");

			delta_x_sign = (delta_x > 0.0f);
			delta_y_sign = (delta_y > 0.0f);

			delta_x *= delta_x;
			delta_y *= delta_y;

			if ( (delta_x > 0.5f) || (delta_y > 0.5f) )
			{
				// horizonal
				if (delta_x > delta_y) {
					SetDirection (delta_x_sign ? IntVector2.right : IntVector2.left);
				// vertical
				} else {
					SetDirection (delta_y_sign ? IntVector2.up : IntVector2.down);
				}
			}
		}
	}
}
