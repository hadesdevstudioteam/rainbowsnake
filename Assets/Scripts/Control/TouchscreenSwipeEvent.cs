﻿using UnityEngine;
using System.Collections;

namespace Control
{
    public class TouchscreenSwipeEvent : Event
    {

        private IntVector2 fp; // first finger position
        private IntVector2 lp; // last finger position
		
        private int deltaX;	// delta X-coordinate
        private int deltaY; // delta Y-coordinate

        private bool axisX; // direction X-coordinate (true - left, false - right)
        private bool axisY; // direction X-coordinate (true - up, false - down)

        protected override void GetEvent ()
        {
            Touch touch;
            int touchCount = Input.touchCount;

            for (int i = 0; i < touchCount; ++i) {
                touch = Input.GetTouch (i);

                switch (touch.phase) {
                case TouchPhase.Began:
					// reset first and last position
                    fp = touch.position;
                    lp = fp;
                    break;

                case TouchPhase.Moved:
					// remember last position
                    lp = touch.position;
                    break;

                case TouchPhase.Ended:

					// get detlas
                    deltaX = fp.x - lp.x;
                    deltaY = fp.y - lp.y;

					// check direction
                    axisX = (deltaX > 0);
                    axisY = (deltaY > 0);

					// for lose sign
                    deltaX *= deltaX;
                    deltaY *= deltaY;
					 
					// if deltas very small
                    if ((deltaX < 25 * 25) && (deltaY < 25 * 25))
                        break;

                    SetDirection (
						(deltaX > deltaY)
						? (axisX)
							? IntVector2.left
							: IntVector2.right
						: (axisY)
							? IntVector2.down
							: IntVector2.up
                    );

                    break;
                }
            }
        }
    }
}