﻿using UnityEngine;
using System.Collections;

namespace Control
{
	public class MouseEvent : Event {

		private const float MOUSE_THRESHOLD = 1.0f;

		protected override void GetEvent()
		{
			if (Input.GetButton("Fire1"))
			{
				float mousex = Input.GetAxis("Mouse X");
				float mousey = Input.GetAxis("Mouse Y");
				if (Mathf.Abs(mousex) > Mathf.Abs(mousey)) {
					if (mousex > MOUSE_THRESHOLD)
						SetDirection(IntVector2.right);
					else if (mousex < -MOUSE_THRESHOLD)
						SetDirection(IntVector2.left);
				}
				else {
					if (mousey > MOUSE_THRESHOLD)
						SetDirection(IntVector2.up);
					else if (mousey < -MOUSE_THRESHOLD)
						SetDirection(IntVector2.down);
				}
			}
		}
	}
}