﻿using UnityEngine;
using System.Collections;

namespace Control
{
	public class TouchscreenMoveEvent : Event {

		private IntVector2 dp; // delta from last finger position

		private Touch touch;

		private int deltaX;	// delta X-coordinate
		private int deltaY; // delta Y-coordinate

		private bool axisX; // direction X-coordinate (true - right, false - left)
		private bool axisY; // direction X-coordinate (true - down, false - up)

		protected override void GetEvent()
		{
			int touchCount = Input.touchCount;

			// no events
			if (touchCount == 0)
				return;

			touch = Input.GetTouch(0);

			// skip if start, end or cancel touch
			if (touch.phase != TouchPhase.Moved)
				return;

			dp = touch.deltaPosition;

			// get detlas
			deltaX = dp.x;
			deltaY = dp.y;

			// check direction
			axisX = (deltaX > 0);
			axisY = (deltaY > 0);

			// for lose sign
			deltaX *= deltaX;
			deltaY *= deltaY;
			
			// if deltas enought
			if ((deltaX < 10) && (deltaY < 10))
				return;

			SetDirection(
				(deltaX > deltaY)
				? (axisX)
					? IntVector2.right
					: IntVector2.left
				: (axisY)
					? IntVector2.up
					: IntVector2.down
			);
		}
	}
}