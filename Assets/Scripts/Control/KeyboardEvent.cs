﻿using UnityEngine;
using System.Collections;

namespace Control
{
	public class KeyboardEvent : Event {

		protected override void GetEvent()
		{
			if (Input.anyKey)
			{
				if (Input.GetKey(KeyCode.LeftArrow))
					SetDirection(IntVector2.left);

				else if (Input.GetKey(KeyCode.RightArrow))
					SetDirection(IntVector2.right);
				
				else if (Input.GetKey(KeyCode.UpArrow))
					SetDirection(IntVector2.up);
				
				else if (Input.GetKey(KeyCode.DownArrow))
					SetDirection(IntVector2.down);
			}
		}
	}
}
