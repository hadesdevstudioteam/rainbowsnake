﻿using UnityEngine;
using System.Collections;

namespace Control
{
    public abstract class Event : MonoBehaviour
    {

        private IntVector2 direction;

        // Use this for initialization
        void Start ()
        {
            direction = IntVector2.zero;
        }
		
        // Update is called once per frame
        void Update ()
        {
            GetEvent ();
        }

        public static Event CreateEvent (Strings.Options.Control.TYPE controlType)
        {
            GameObject eventMockObject = new GameObject (Strings.GameObjects.EVENT_MOCK_OBJECT);
            switch (controlType) {
            case Strings.Options.Control.TYPE.KEYBOARD:
                return eventMockObject.AddComponent<KeyboardEvent> ();
            case Strings.Options.Control.TYPE.MOUSE:
                return eventMockObject.AddComponent<MouseEvent> ();
            case Strings.Options.Control.TYPE.TOUCH_MOVE:
                return eventMockObject.AddComponent<TouchscreenMoveEvent> ();
            case Strings.Options.Control.TYPE.TOUCH_SWIPE:
                return eventMockObject.AddComponent<TouchscreenSwipeEvent> ();
            case Strings.Options.Control.TYPE.JOYSTICK:
                return eventMockObject.AddComponent<JoystickEvent> ();
            }
            return null;
        }

        protected void SetDirection (IntVector2 newDirection)
        {
            direction = newDirection;
        }

        public IntVector2 GetValidDirection (IntVector2 oldDirection)
        {
            // if moving up, disable moving down
            if (oldDirection == IntVector2.up) {
                return (direction != IntVector2.down) ? direction : oldDirection;
                // if moving down, disable moving up
            } else if (oldDirection == IntVector2.down) {
                return (direction != IntVector2.up) ? direction : oldDirection;
                // if moving left, disable moving right
            } else if (oldDirection == IntVector2.left) {
                return (direction != IntVector2.right) ? direction : oldDirection;
                // if moving right, disable moving left
            } else if (oldDirection == IntVector2.right) {
                return (direction != IntVector2.left) ? direction : oldDirection;
                // if start game
            } else if (direction == IntVector2.zero) {
                return oldDirection;
                // if start game
            } else if (oldDirection == IntVector2.zero) {
                return direction;
            }
            return oldDirection;
        }

        protected abstract void GetEvent ();
    }
}