﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class GradientBarScript : MonoBehaviour
{

    private static Texture2D texture;
    public Slider slider;

    public void OnLevelChanged (Gradient gradient)
    {
        texture = createGradient (1000, 1, gradient);
        try {
            //overrideSprite - это официальный костыль, т.к. texture поле нельзя изменять
            Sprite s = Sprite.Create (texture, new Rect (0f, 0f, 1000f, 1f), Vector2.zero, 100f);
            gameObject.GetComponent<Image> ().overrideSprite = s;
        } catch (Exception e) {
            print (e.Message);
        }
    }

    private Texture2D createGradient (int width, int height, Gradient gradient)
    {
        Texture2D ret = new Texture2D (width, height);

        float width_f = width - 1;
        for (int x = 0; x < width; ++x) {
            float t = (float)x / width_f;
            Color color = gradient.Evaluate (t);
 
            for (int y = 0; y < height; ++y) {
                ret.SetPixel (x, y, color);
            }
        }
        ret.Apply ();
        return ret;
    }

    public void OnSnakeLengthChanged (float currentEvaluate)
    {
        slider.value = currentEvaluate;
    }
}
