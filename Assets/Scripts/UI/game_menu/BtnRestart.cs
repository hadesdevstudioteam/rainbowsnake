﻿using UnityEngine;
using System.Collections;

public class BtnRestart : SFXButton
{
    public override void OnClick()
    {
        base.OnClick();
        Application.LoadLevel(Strings.Scenes.GAME);
    }
}
