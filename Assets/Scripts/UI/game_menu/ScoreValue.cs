﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreValue : MonoBehaviour
{
    public Text scoreValue;

    private int score;

    void Start()
    {
        score = PlayerPrefs.GetInt(Strings.Prefs.FINAL_SCORE);
        scoreValue.text = score.ToString();
    }

    void FixedUpdate()
    {
        score = PlayerPrefs.GetInt(Strings.Prefs.FINAL_SCORE);
        scoreValue.text = score.ToString();
    }
}
