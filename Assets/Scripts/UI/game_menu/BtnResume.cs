﻿using UnityEngine;
using System.Collections;

public class BtnResume : SFXButton
{

    public override void OnClick()
    {
        base.OnClick();
        LevelManager.I.HideMenu();
    }
}
