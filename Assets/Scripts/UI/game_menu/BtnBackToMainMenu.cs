﻿using UnityEngine;
using System.Collections;

public class BtnBackToMainMenu : SFXButton
{
    public override void OnClick()
    {
        base.OnClick();
        Application.LoadLevel(Strings.Scenes.LOBBY);
    }
}
