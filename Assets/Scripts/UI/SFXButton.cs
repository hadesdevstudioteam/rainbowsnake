﻿using UnityEngine;
using System.Collections;

public class SFXButton : MonoBehaviour
{
  public virtual void OnClick()
  {
    SFXSingleton.Get().PlayButton();
  }
}
