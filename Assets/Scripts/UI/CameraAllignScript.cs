﻿using UnityEngine;
using System.Collections;

/**
 * Привязывает камеру к левому нижнему углу игровой зоны.
 * Масштабирует камеру под размеры игрового поля.
*/
[RequireComponent (typeof (Camera))]
public class CameraAllignScript : MonoBehaviour
{
    float currentAspect = 0;

    void Update() {
        if (currentAspect != GetComponent<Camera>().aspect)
        {
            GetComponent<Camera>().transform.position = Vector3.zero;
            Vector3 rightScreenPosition = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, -10f));
            Vector3 trans = new Vector3(11f - rightScreenPosition.x, GetComponent<Camera>().orthographicSize, -10f);
            GetComponent<Camera>().gameObject.transform.position = trans;
            currentAspect = GetComponent<Camera>().aspect;
        }
    }
}
