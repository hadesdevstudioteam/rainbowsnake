﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class SoundSfxOption : MonoBehaviour
{
	bool isInitialized = false;

	// Use this for initialization
	void Start()
	{
		bool isOn = PlayerPrefs.GetInt(Strings.Options.Sound.SFX) == 0 ? false : true;
		GetComponent<Toggle>().isOn = isOn;
		isInitialized = true;
	}

	public void OnMusicValueChanged(bool isOn)
	{
		if (isInitialized)
		{
			SFXSingleton.Get().PlayButton();
			SFXSingleton.Get().GetComponent<AudioSource>().enabled = isOn;
			PlayerPrefs.SetInt(Strings.Options.Sound.SFX, isOn ? 1 : 0);
		}
	}
}
