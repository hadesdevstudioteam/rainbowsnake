﻿using UnityEngine;
using System.Collections;

public class BtnBackFromOptions : SFXButton
{

  public GameObject lobbyCanvas;
  public GameObject optionCanvas;

  public override void OnClick()
  {
    base.OnClick();
    if (lobbyCanvas != null && optionCanvas != null)
    {
      optionCanvas.SetActive(false);
      lobbyCanvas.SetActive(true);
    }
  }
}
