﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SnakeSpeed : MonoBehaviour
{
  Text speedText;
  bool isInitialized = false;

  void Start()
  {
    float speed = PlayerPrefs.GetFloat(Strings.Options.Snake.SPEED);
        
    // check correct saved snake speed
    if ((speed < Strings.Options.Snake.Speed.MIN_VALUE) ||
      (speed > Strings.Options.Snake.Speed.MAX_VALUE))
    {
      speed = Strings.Options.Snake.Speed.DEF_VALUE;
      PlayerPrefs.SetFloat(Strings.Options.Snake.SPEED, speed);
    }

    speedText = gameObject.GetComponentInChildren<Text>();
    Slider slider = GetComponent<Slider>();
    slider.minValue = Strings.Options.Snake.Speed.MIN_VALUE;
    slider.maxValue = Strings.Options.Snake.Speed.MAX_VALUE;
    slider.value = speed;

    speedText.text = "speed : " + speed;
    isInitialized = true;
  }

  public void SnakeSpeedOptionChanged(float speed)
  {
    if (isInitialized)
    {
      SFXSingleton.Get().PlayEat();
      speedText.text = "speed : " + speed;
      PlayerPrefs.SetFloat(Strings.Options.Snake.SPEED, speed);
    }
  }
}
