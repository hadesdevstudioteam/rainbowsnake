﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//листнер чекбокса ReverseColor
public class SnakeReversColor : MonoBehaviour
{
  private bool isInitialized = false;

  void Start()
  {
    bool isOn = (PlayerPrefs.GetInt(Strings.Options.Snake.COLOR_REVERSE) == 0);
    gameObject.GetComponent<Toggle>().isOn = isOn;
    isInitialized = true;
  }

  public void SetActiveReverse(bool value)
  {
    if (isInitialized)
    {
      SFXSingleton.Get().PlayButton();
      PlayerPrefs.SetInt(Strings.Options.Snake.COLOR_REVERSE, value ? 0 : 1);
    }
  }
}
