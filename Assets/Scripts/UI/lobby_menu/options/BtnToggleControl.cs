﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BtnToggleControl : MonoBehaviour
{
  public Strings.Options.Control.TYPE controlType;
  bool isInitialized = false;

  void Start()
  {
    gameObject.GetComponent<Toggle>().isOn = 
            controlType == (Strings.Options.Control.TYPE)PlayerPrefs.GetInt(Strings.Options.CONTROL);
    isInitialized = true;
  }

  public void OnClick(bool isOn)
  {
    if (isInitialized)
    {
      SFXSingleton.Get().PlayButton();
      if (isOn)
      {
        PlayerPrefs.SetInt(Strings.Options.CONTROL, (int)controlType);
      }
    }
  }
}
