﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class SoundMusicOption : MonoBehaviour
{
	bool isInitialized = false;

	// Use this for initialization
	void Start()
	{
		bool isOn = PlayerPrefs.GetInt(Strings.Options.Sound.MUSIC) == 0 ? false : true;
		GetComponent<Toggle>().isOn = isOn;
		isInitialized = true;
	}

	public void OnMusicValueChanged(bool isOn)
	{
		if (isInitialized)
		{
			SFXSingleton.Get().PlayButton();
			MusicSingleton.Get().GetComponent<AudioSource>().enabled = isOn;
			PlayerPrefs.SetInt(Strings.Options.Sound.MUSIC, isOn ? 1 : 0);
		}
	}
}
