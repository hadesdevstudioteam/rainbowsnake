﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class BtnOptions : SFXButton
{

    public GameObject lobbyCanvas;
    public GameObject optionCanvas;
    public EventSystem eventSystem;
    public Button buttonToSelect;

    public override void OnClick()
    {
        base.OnClick();
        if (lobbyCanvas != null && optionCanvas != null && eventSystem != null && buttonToSelect != null)
        {
            lobbyCanvas.SetActive(false);
            optionCanvas.SetActive(true);
            eventSystem.SetSelectedGameObject(buttonToSelect.gameObject);
        } else {
            Debug.LogError("some object is null");
        }
    }
}
