﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BtnContinue : SFXButton
{
    public override void OnClick()
    {
        // reset final score on 1st level
        if (PlayerPrefs.GetInt(Strings.Prefs.LEVEL) == Strings.Prefs.Level.DEF_VALUE)
        {
            PlayerPrefs.SetInt(Strings.Prefs.FINAL_SCORE, Strings.Prefs.FinalScore.DEF_VALUE);
        }

        base.OnClick();
        Application.LoadLevel(Strings.Scenes.GAME);
    }
}
