﻿using UnityEngine;
using System.Collections;

public class ExitButtonListener : MonoBehaviour
{
	void OnApplicationQuit() {
		Application.Quit();
	}

	void OnGUI(){
		if ((Application.platform != RuntimePlatform.WindowsPlayer)
		    && Input.GetKey(KeyCode.Escape)) Application.Quit();
	}
}
