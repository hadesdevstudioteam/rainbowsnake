﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Slider))]
public class LifeValue : MonoBehaviour
{
  public Text lifeValue;
  Slider slider;
  
  void Start()
  {
    slider = GetComponent<Slider>();
    slider.minValue = Strings.Prefs.Life.MIN_VALUE;
    slider.maxValue = Strings.Prefs.Life.MAX_VALUE;
    FixedUpdate();
  }
  
  void FixedUpdate()
  {
    int life = PlayerPrefs.GetInt(Strings.Prefs.LIFE);
    lifeValue.text = life.ToString();
    slider.value = life;
  }
}
