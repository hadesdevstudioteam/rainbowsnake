﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Slider))]
public class LevelValue : MonoBehaviour
{
  public Text levelValue;
  Slider slider;
  
  void Start()
  {
    slider = GetComponent<Slider>();
    slider.minValue = Strings.Prefs.Level.MIN_VALUE;
    slider.maxValue = Strings.Prefs.Level.MAX_VALUE;
    FixedUpdate();
  }
  
  void FixedUpdate()
  {
    int level = PlayerPrefs.GetInt(Strings.Prefs.LEVEL);
    levelValue.text = level.ToString();
    slider.value = level;
  }
}
