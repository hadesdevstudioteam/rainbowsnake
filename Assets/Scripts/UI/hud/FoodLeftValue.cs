﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Slider))]
public class FoodLeftValue : MonoBehaviour
{
  public Text foodLeftValue;
  Slider slider;

  void Awake()
  {
    slider = GetComponent<Slider>();
    slider.minValue = 1;
    slider.maxValue = 0;
  }

  // Update is called once per frame
  public void Init(int maxFood)
  {
    slider.maxValue = maxFood;
    slider.value = maxFood;
    foodLeftValue.text = slider.value.ToString();
  }

  public void UpdateFood(int currentValue)
  {
    slider.value = currentValue;
    foodLeftValue.text = (slider.maxValue - currentValue).ToString();
  }
}
