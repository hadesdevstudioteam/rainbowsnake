using UnityEngine;

public struct IntVector2 {

    public int x;
    public int y;

    public IntVector2(int x, int y) {
        this.x = x;
        this.y = y;
		vec2 = new Vector2();
		vec3 = new Vector3();
    }

    // keep allocated Vector2 and Vector3 for improve access
    private Vector2 vec2;
    private Vector3 vec3;

    private Vector2 ToVector2() {
        vec2.x = x;
        vec2.y = y;
        return vec2;
    }

    private Vector3 ToVector3()
    {
        vec3.x = x;
        vec3.y = y;
        vec3.z = 0;
        return vec3;
    }

    public Vector2 MoveTo(float x, float y) {
        vec2 = ToVector2();
        vec2.x += x;
        vec2.y += y;
        return vec2;
    }
    public Vector3 MoveTo(float x, float y, float z)
    {
        vec3 = ToVector3();
        vec3.x += x;
        vec3.y += y;
        vec3.z += z;
        return vec3;
    }

    // allocate vector for improve access
    static IntVector2 __zero = new IntVector2(0,0);
    static IntVector2 __right = new IntVector2(1,0);
    static IntVector2 __left = new IntVector2(-1,0);
    static IntVector2 __up = new IntVector2(0,1);
    static IntVector2 __down = new IntVector2(0,-1);

    public static IntVector2 zero { get { return __zero; } }
    public static IntVector2 right { get { return __right; } }
    public static IntVector2 left { get { return __left; } }
    public static IntVector2 up { get { return __up; } }
    public static IntVector2 down { get { return __down; } }

    public static bool operator !=(IntVector2 lhs, IntVector2 rhs)
    {
        return (lhs.x != rhs.x) || (lhs.y != rhs.y);
    }

    public static bool operator ==(IntVector2 lhs, IntVector2 rhs)
    {
        return (lhs.x == rhs.x) && (lhs.y == rhs.y);
    }

    public static implicit operator Vector2(IntVector2 v)
    {
        return v.ToVector2();
    }

    public static implicit operator Vector3(IntVector2 v)
    {
        return v.ToVector3();
    }

    public static implicit operator IntVector2(Vector2 v)
    {
		return new IntVector2((int)v.x, (int)v.y);
    }

    public static implicit operator IntVector2(Vector3 v)
    {
		return new IntVector2((int)v.x, (int)v.y);
    }

	public override int GetHashCode ()
	{
		return this.x.GetHashCode () ^ this.y.GetHashCode () << 2;
	}

    public override bool Equals (object other)
    {
        if (!(other is IntVector2))
        {
            return false;
        }
        IntVector2 vector = (IntVector2)other;
		return this.x.Equals (vector.x) && this.y.Equals (vector.y);
    }
}
