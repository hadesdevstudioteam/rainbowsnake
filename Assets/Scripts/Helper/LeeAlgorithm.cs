using System;

public class LeeAlgorithm
{
	// Lee Algorithm
	private int[,] matrix;
	private int[,] find_matrix;
	private static int FREE = -2;
	private static int BUSY = 999;
	private static int[] dx = new int[4] { 1, 0, -1, 0};
	private static int[] dy = new int[4] { 0, 1, 0, -1};

	private int col, row;
	
	// только для ускорения сравнения
	private int last_col;
	private int last_row;

	public LeeAlgorithm(int col, int row)
	{
		this.col = col;
		this.row = row;
		this.last_col = col-1;
		this.last_row = row-1;
		// Warning! Rows and cols swap
		matrix = new int[row, col];
		// временная матрица для просчета пути
		find_matrix = new int[row, col];
	}

	public void SetCellAsFree(int x, int y)
	{
		// Warning! Rows and cols swap
		matrix[y, x] = FREE;
	}
	
	public void SetCellAsBusy(int x, int y)
	{
		// Warning! Rows and cols swap
		matrix[y, x] = BUSY;
	}

	public bool find(int ax, int ay, int bx, int by)
	{
		// распространение волны
		int d = 0;

		Array.Copy(matrix, find_matrix, col * row);

		// помечаем стартовую ячейку как 0 (начало волны)
		find_matrix[ay, ax] = 0;

		// помечаем целевую ячейку как FREE
		find_matrix[by, bx] = FREE;

		bool stop;

		do {
			// предполагаем, что все свободные клетки уже помечены
			stop = true;
			
			for ( int y = 0; y < row; ++y )
			{
				for ( int x = 0; x < col; ++x )
				{
					// ячейка (x, y) помечена числом d
					if ( find_matrix[y, x] == d )
					{
						// проходим по всем непомеченным соседям
						for ( int k = 0; k < 4; ++k )
						{
							// проверяем выход за пределы массива
							if (
								// первый вектор вправо (1,0) - проверяем k = 0 и x = col-1
								( (k == 0) && (x == last_col) ) ||
								// второй вектор вниз (0,1) - проверяем k = 1 и y = row-1
								( (k == 1) && (y == last_row) ) ||
								// третий вектор влево (-1,0) - проверяем k = 2 и x = 0
								( (k == 2) && (x == 0) ) ||
								// четвертый вектор вверх (0,-1) - проверяем k = 3 и y = 0
								( (k == 3) && (y == 0) ) )
								continue;

							if ( find_matrix[y + dy[k], x + dx[k]] == FREE )
							{
								// найдены непомеченные клетки
								stop = false;
								// распространяем волну
								find_matrix[y + dy[k], x + dx[k]] = d + 1;
							}
						}
					}
				}
			}
			++d;
		} while ( !stop && find_matrix[by, bx] == FREE );

		// true - путь найден
		// false - путь не найден
		return (find_matrix[by, bx] != FREE);
	}
}