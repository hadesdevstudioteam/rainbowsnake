﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Singleton<T> : MonoBehaviour
  where T : UnityEngine.Component
{
  public static T Get()
  {
    GameObject go = GameObject.FindGameObjectWithTag(typeof(T).Name);
    if (go != null)
    {
      return go.GetComponent<T>();
    }
    return default(T);
  }
}

public class SingletonSpawner : MonoBehaviour
{
  public List<GameObject> singletons;

  void Start()
  {
    foreach (GameObject singleton in singletons)
    {
      GameObject go = GameObject.FindGameObjectWithTag(singleton.tag);
      if (go == null)
      {
        go = GameObject.Instantiate(singleton) as GameObject;
        GameObject.DontDestroyOnLoad(go);
      }
    }

    Destroy(gameObject);
  }

}
